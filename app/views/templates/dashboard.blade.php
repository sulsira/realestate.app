@section('styles')
    {{ HTML::style('css/sticky.css') }}
    {{ HTML::style('css/scheme.css') }}
    {{ HTML::style('css/style.css') }}
    {{ HTML::style('css/dashboard.css') }}
@stop
@section('header')
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <img src="..." alt="..." class="img-circle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
<ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img src="..." alt="..." class="img-circle"> Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
@stop
@section('sidebar')
  <div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
      <li class="active"><a href="http://getbootstrap.com/examples/dashboard/#">Department <span class="sr-only">(current)</span></a></li>
      <li><a href="http://getbootstrap.com/examples/dashboard/#">admin</a></li>
      <li><a href="http://getbootstrap.com/examples/dashboard/#"><img src="..." alt="..." class="img-circle"></a></li>
      <li><a href="http://getbootstrap.com/examples/dashboard/#">User fullname</a></li>
    </ul>
    <hr>
    <ul class="nav nav-sidebar">
      <li><a href="">Nav item</a></li>
      <li><a href="">Nav item again</a></li>
      <li><a href="">One more nav</a></li>
      <li><a href="">Another nav item</a></li>
      <li><a href="">More navigation</a></li>
    </ul>
    <hr>
    <ul class="nav nav-sidebar">
      <li><a href="">Menu view</a></li>
    </ul>
  </div>
@stop
@section('footer')
  @include('__partials/footer')
@stop