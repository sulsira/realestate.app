@section('styles')
    {{ HTML::style('css/sticky.css') }}
    {{ HTML::style('css/login.css') }}
@stop

@section('header')
	<header>
		<h1>real properties</h1>
      <h4>property management system v0.7 </h4><span>(PMS V0.7)</span>
      <hr>
	</header>
@stop
@section('sidebar')

@stop
	
@section('footer')
  @include('__partials/footer')
@stop