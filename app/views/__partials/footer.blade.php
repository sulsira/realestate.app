<footer>
	<div class="container">
		<ul>
			<li><?php echo date('Y'); ?> @ copyright</li>
			<li><a href="#">developers</a></li>
			<li><a href="#">privacy</a></li>
		</ul>
	</div>
</footer>
<!-- scripts -->
{{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js')}}
{{HTML::script('js/bootstrap.js')}}
{{HTML::script('js/npm.js')}}
{{HTML::script('js/ie10-viewport-bug-workaround.js')}}

