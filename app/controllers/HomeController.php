<?php

class HomeController extends BaseController {

    protected $layout = 'layouts/login';
    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function showWelcome()
	{
		$this->layout->content = View::make('index');
	}

/**
 * Displave the login form pager.
 *
 * @return void
 */
public function showLogin()
{
    $this->layout->content = View::make('login');
}

}
